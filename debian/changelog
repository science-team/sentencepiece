sentencepiece (0.2.0-1) unstable; urgency=medium

  * New upstream version 0.2.0
  * debian/patches/fix-ftbfs-big-endian.patch
    debian/patches/header-dependencies.patch
    - Drop needless patch which is aleady fixed in upstream.

 -- Kentaro Hayashi <kenhys@xdump.org>  Fri, 23 Feb 2024 20:23:45 +0900

sentencepiece (0.1.99-4) unstable; urgency=medium

  * debian/clean
    - Fix FTBFS (double build) (Closes: #1047552)

 -- Kentaro Hayashi <kenhys@xdump.org>  Sat, 19 Aug 2023 17:14:56 +0900

sentencepiece (0.1.99-3) unstable; urgency=medium

  * debian/tests/control
    - Fix regression (preventing migration) about
      python module's autopkgtest.

 -- Kentaro Hayashi <kenhys@xdump.org>  Wed, 21 Jun 2023 14:44:27 +0900

sentencepiece (0.1.99-2) unstable; urgency=medium

  * debian/patches/fix-ftbfs-big-endian.patch
    - Add patch to fix FTBFS on big endian platform.
  * debian/tests/control
    - Fix W: illegal-runtime-test-name warning

 -- Kentaro Hayashi <kenhys@xdump.org>  Tue, 20 Jun 2023 19:28:50 +0900

sentencepiece (0.1.99-1) unstable; urgency=medium

  * New upstream version 0.1.99
  * debian/control
    - Bump Standards-Version to 4.6.2. No other changes are required.
  * debian/patches/disable-static-library.patch
    debian/patches/support-python-module-in-place.patch
    - Refresh patch files for 0.1.99
  * debian/patches/*.patch
    - Drop deprecated patch files which was already applied in upstream.
  * debian/README.Debian
    - Update explanation of debian/patches.

 -- Kentaro Hayashi <kenhys@xdump.org>  Sun, 18 Jun 2023 00:04:54 +0900

sentencepiece (0.1.97-3) unstable; urgency=medium

  * debian/patches/0001-update-python-wrapper.patch
    debian/patches/0002-remove-debug-symbols-from-wheel-package.patch
    debian/patches/0003-allow-tab-character-to-be-used-in-user_defined_symbo.patch
    debian/patches/0004-add-test-to-use-tab-as-user-defined-symbols.patch
    debian/patches/0005-Uses-C-17-by-default.patch
    debian/patches/0006-Uses-std-atomic-to-define-global-variable.patch
    debian/patches/0007-Fix-a-typo.patch
    debian/patches/0008-Uses-absl-string_view-as-much-as-possible.patch
    debian/patches/0009-Fixed-build-break.patch
    debian/patches/0010-Added-ImmutableSentencePiece-class.patch
    debian/patches/0011-add-verbose-option.patch
    debian/patches/0012-Supports-ImmutableSentencePieceText-from-python-modu.patch
    debian/patches/0013-Adds-more-unittests.patch
    debian/patches/0014-Adds-SWIGPYTHON-flag.patch
    debian/patches/0015-remove-unused-ifdef-SWIG-macro.patch
    debian/patches/0016-Fixed-test-failure.patch
    debian/patches/0017-Uses-property-in-immutable-proto.patch
    debian/patches/0018-automatically-detect-the-number-of-CPUs-in-batch-pro.patch
    debian/patches/0019-support-slice-in-pieces-nbests-objects.patch
    debian/patches/0020-Updated-the-document.patch
    debian/patches/0021-Fixed-errors-in-example-notebook.patch
    debian/patches/0022-Fix-dead-links.patch
    debian/patches/0023-added-ShutdownLibrary-function-to-uninitialize-globa.patch
    debian/patches/0024-Fixed-the-issue-of-concatinating-paths-for-pkg-confi.patch
    - Add missing patch files for 0.1.97.
  * debian/README.Debian
    - Add explanation of debian/patches.

 -- Kentaro Hayashi <kenhys@xdump.org>  Mon, 21 Nov 2022 22:43:46 +0900

sentencepiece (0.1.97-2) unstable; urgency=medium

  * Team upload

  [ Steve Langasek ]
  * debian/patches/header-dependencies.patch: include necessary headers
    to ensure IS_BIG_ENDIAN is defined, see #1017360.

 -- Graham Inggs <ginggs@debian.org>  Sun, 18 Sep 2022 05:30:57 +0000

sentencepiece (0.1.97-1) unstable; urgency=medium

  * New upstream version 0.1.97
  * debian/copyright
    - Update maintainer E-mail address
  * debian/control
    - Bump Standards-Version to 4.6.1. No other changes are required.
  * debian/patches/support-python-module-in-place.patch
    - Refresh path to build python module.

 -- Kentaro Hayashi <kenhys@xdump.org>  Tue, 14 Jun 2022 20:19:58 +0900

sentencepiece (0.1.96-1) unstable; urgency=medium

  * New upstream version 0.1.96
  * debian/control
    - Bump standard-version to 4.5.1. No changes are required.

 -- Kentaro Hayashi <kenhys@xdump.org>  Wed, 18 Aug 2021 20:52:46 +0900

sentencepiece (0.1.95-1) unstable; urgency=medium

  * New upstream version 0.1.95
  * debian/patches/support-python-module-in-place.patch
    - Fix undefined symbol when importing python module (Closes: #979040)

 -- Kentaro Hayashi <kenhys@xdump.org>  Thu, 11 Feb 2021 17:36:23 +0900

sentencepiece (0.1.94-2) unstable; urgency=medium

  * Fix FTBFS on armel/mipsel (Closes: #977235)

 -- Kentaro Hayashi <kenhys@xdump.org>  Wed, 16 Dec 2020 21:18:15 +0900

sentencepiece (0.1.94-1) unstable; urgency=medium

  * New upstream version 0.1.94
  * debian/patches/support-python-module-in-place.patch
    - Refresh path to build python module.
  * debian/patches/fix-ftbfs-ports.patch
    debian/patches/mutiarch-support.patch
    - Remove needless patch because these patch was merged
      to google/sentencepiece.

 -- Kentaro Hayashi <kenhys@xdump.org>  Wed, 28 Oct 2020 21:02:07 +0900

sentencepiece (0.1.93-1) unstable; urgency=medium

  * New upstream version 0.1.93
  * debian/source/lintian-overrides
    - Remove needless override.

 -- Kentaro Hayashi <kenhys@xdump.org>  Thu, 15 Oct 2020 21:32:05 +0900

sentencepiece (0.1.92-3) unstable; urgency=medium

  * debian/patches/fix-ftbfs-ports.patch
    - Fix FTBFS on powerpc

 -- Kentaro Hayashi <kenhys@xdump.org>  Sat, 03 Oct 2020 20:48:27 +0900

sentencepiece (0.1.92-2) unstable; urgency=medium

  * debian/patches/0002-Change-in-order-to-build-Python-modules-in-place.patch
    - Fix FTBFS on hurd-i386
  * debian/patches/0004-Fix-FTBFS-on-armel-and-mipsel.patch
    - Fix missing dependency to atomic library (powerpc,m68k,sh4)

 -- Kentaro Hayashi <kenhys@xdump.org>  Sat, 26 Sep 2020 20:27:17 +0900

sentencepiece (0.1.92-1) unstable; urgency=medium

  * New upstream version 0.1.92

 -- Kentaro Hayashi <kenhys@xdump.org>  Fri, 19 Jun 2020 19:38:49 +0900

sentencepiece (0.1.91-1) unstable; urgency=medium

  * New upstream version 0.1.91

 -- Kentaro Hayashi <kenhys@xdump.org>  Fri, 22 May 2020 15:17:42 +0900

sentencepiece (0.1.90-3) unstable; urgency=medium

  * debian/patches/0004-Fix-FTBFS-on-armel-and-mipsel.patch
    - Refresh patch to fix FTBFS.

 -- Kentaro Hayashi <kenhys@xdump.org>  Sun, 17 May 2020 09:02:23 +0900

sentencepiece (0.1.90-2) unstable; urgency=medium

  * debian/patches/0004-Fix-FTBFS-on-armel-and-mipsel.patch
    - Add patch to fix FTBFS on mipsel and armel

 -- Kentaro Hayashi <kenhys@xdump.org>  Sat, 16 May 2020 16:16:45 +0900

sentencepiece (0.1.90-1) unstable; urgency=medium

  * New upstream version 0.1.90
  * debian/control
    - Update Uploaders:
    - Bump standard-version to 4.5.0
    - Bump compat version to 13.
  * debian/source/lintian-overrides
    - Fix false positive source-is-missing
  * debian/patches/0003-Disable-static-library-explicitly.patch
    - Disable to build static library

 -- Kentaro Hayashi <kenhys@xdump.org>  Wed, 13 May 2020 19:09:34 +0900

sentencepiece (0.1.84-1) unstable; urgency=medium

  * New upstream version 0.1.84 (Closes: #939860)

  [ TSUCHIYA Masatoshi ]
  * Initial packaging tasks.
  * Remove pipeline configurations for BitBucket.

  [ Kentaro Hayashi ]
  * debian/gbp.conf
    - Add basic configuration about debian-branch
  * debian/watch
    - Add missing watch file to detect a new release
  * debian/control
    - Update deprecated Priority: to optional
    - Add Vcs-* fields
    - Fix W: sentencepiece: description-synopsis-starts-with-article
    - Bump standard version to 4.4.1
    - Update Vcs-* under science-team
    - Bump up compatibility level
    - Drop python2 support
  * debian/copyright
    - Use https://
    - Update copyright about third party modules
  * debian/rules
    - Enable hardening
  * debian/salsa-ci.yml
    - Add Salsa CI configuration

 -- Kentaro Hayashi <hayashi@clear-code.com>  Thu, 17 Oct 2019 13:33:34 +0900
